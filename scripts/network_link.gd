extends Node
class_name NetworkLink

var from = null
var to = null
var tiles = []


func _init(var i_network_link_resource):
	assert(i_network_link_resource is NetworkLinkResource)
	from = i_network_link_resource.from
	to = i_network_link_resource.to
	tiles = i_network_link_resource.tiles


func _ready():
	pass

#func _process(delta):
#	pass

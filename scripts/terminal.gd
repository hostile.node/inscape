extends RichTextLabel
class_name Terminal

var inbuilt_commands = null
var consoleText = ""
var ps1 = "[gateway /]$ "
var currentCommand = ""


# Called when the node enters the scene tree for the first time.
func _ready():
	inbuilt_commands = get_node("InbuiltCommands")
	_greeting()
	pass # Replace with function body.


func _input(event):
	if event is InputEventKey && event.pressed:
		if is_printable(event):
			currentCommand += get_printable_char(event)
			show_prompt()
		elif event.scancode == KEY_ENTER || event.scancode == KEY_KP_ENTER:
			execute_command(currentCommand)
			show_prompt()
		elif event.scancode == KEY_BACKSPACE:
			backspace()
			show_prompt()


func add_line(line = ""):
	consoleText += line + "\n"
	show_prompt()
	
	
func add_line_error(line = ""):
	add_line("[color=red]" + line + "[/color]")
	
	
func add_line_warning(line = ""):
	add_line("[color=orange]" + line + "[/color]")


func backspace():
	var l = currentCommand.length()
	if l > 0:
		currentCommand.erase(l - 1, 1)


func show_prompt():
	bbcode_text = consoleText + ps1 + currentCommand


func execute_command(command):
	add_line(ps1 + currentCommand)
	
	var command_executed = false
	if currentCommand.length() > 0:
		var split_command = command.split(" ", false)
		var program = split_command[0]
		var arguments = []
		if split_command.size() > 1:
			split_command.remove(0)
			arguments = split_command
		
		if _execute_inbuilt_command(program, arguments) || _execute_program(program, arguments):
			command_executed = true
			
	currentCommand = ""
	if command_executed == false:
		add_line("[color=red]Invalid command.[/color]")
	
	
func _execute_inbuilt_command(program, arguments):
	var inbuilt_command = inbuilt_commands.find_node(program)
	if inbuilt_command != null:
		assert(inbuilt_command is InbuiltCommand)
		inbuilt_command.execute(arguments)
		return true
	else:
		return false


func _execute_program(binary, arguments):
	return false


func is_printable(event):
	var s = event.scancode
	return (s >= KEY_KP_0 && s <= KEY_KP_9) || (s >= KEY_SPACE && s <= KEY_ASCIITILDE)


func get_printable_char(event):
	return char(event.unicode)


func _greeting():
	add_line("Inscape GNU/Linux 0.1.0 gateway tty1")
	add_line()
	add_line("gateway login: user")
	add_line("Password:")
	add_line()
	add_line("The Inscape GNU/Linux system is free software; the source code is available at [url]https://gitlab.com/hostile.node/inscape[/url].")
	add_line()
	add_line("Inscape GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extend permitted by applicable law.")
	add_line()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

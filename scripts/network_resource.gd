extends Resource
class_name NetworkResource

export(String) var network_name
export(float) var trace
export(Array, Resource) var nodes
export(Array, Resource) var links

# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(i_name = "Template network", i_trace = 1.0, i_nodes = [], i_links = []):
	network_name = i_name
	trace = i_trace
	nodes = i_nodes
	links = i_links

extends NinePatchRect

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func on_network_changed(var network):
	var title = get_node("Title")
	
	if network.is_network_connected():
		title.bbcode_text = "[b]//NETWORK_VIEW - " + network.get_name() + "[/b]"
	else:
		title.bbcode_text = "[b]//NETWORK_VIEW[/b]"


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

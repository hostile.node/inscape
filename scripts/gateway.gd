extends Node2D
class_name Gateway

var filesystem = null
var network = null
var terminal = null


# Called when the node enters the scene tree for the first time.
func _ready():
	terminal = find_node("Terminal")

	print("Gateway ready...")
	filesystem = Node.new()
	filesystem.set_script(load("res://scripts/filesystem.gd"))
	add_child(filesystem)

	network = get_node("Network")
	if network.connect_to_network(load("res://networks/astra_experimental.tres")):
		terminal.add_line("Connected to network '" + network.get_name() + "'.")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

extends Node
class_name Network

export(NodePath) var nodes_map_path
export(NodePath) var links_map_path
export(NodePath) var network_view_window_path
export(NodePath) var network_view_camera_path
export(NodePath) var runner_marker_path

var nodes_map : TileMap = null
var links_map : TileMap = null
var network_resource : NetworkResource = null
var network_view_camera = null
var runner_marker = null
var runner_node : NetworkNode = null

var links = []
var nodes = []
var node_labels = []


func is_network_connected():
	return network_resource != null


func connect_to_network(var res : NetworkResource):	
	if is_network_connected():
		printerr("Already connected to network.")
		return false

	network_resource = res
	var network_view_window = get_node(network_view_window_path)
	network_view_window.on_network_changed(self)
	_instantiate_network_nodes()
	_instantiate_network_links()
	_place_runner_at_ingress()
	return true


func get_name():
	if network_resource == null:
		return ""
	else:
		return network_resource.network_name


func get_linked_nodes(var node : NetworkNode):
	var linked_nodes = []
	var nodes_to_resolve = []
	for link in links:
		if link.to == node.label:
			nodes_to_resolve.append(link.from)
		elif link.from == node.label:
			nodes_to_resolve.append(link.to)
			
	for node_to_resolve in nodes_to_resolve:
		for node in nodes:
			if node.label == node_to_resolve:
				linked_nodes.append(node)
				break
			
	return linked_nodes


func move_runner(var node : NetworkNode):
	assert(runner_node != null)
	
	if node == runner_node:
		return true
	
	var linked_nodes = get_linked_nodes(runner_node)
	for linked_node in linked_nodes:
		if linked_node == node:
			_place_runner_at_node(node)
			return true
	
	return false


func _instantiate_network_nodes():
	for node_resource in network_resource.nodes:
		assert(node_resource is NetworkNodeResource)
		var node = NetworkNode.new(node_resource, network_view_camera)
		nodes.append(node)
		add_child(node)


func _instantiate_network_links():
	for link_resource in network_resource.links:
		assert(link_resource is NetworkLinkResource)
		var link = NetworkLink.new(link_resource)
		links.append(link)
		add_child(link)


func _place_runner_at_ingress():
	var ingress_node = null
	for node in nodes:
		if node.type == NetworkNodeResource.NetworkNodeType.INGRESS:
			ingress_node = node
			break
			
	assert(ingress_node != null)
	_place_runner_at_node(ingress_node)


func _place_runner_at_node(var node : NetworkNode):
	var node_position = Vector2(node.tilemap_position.x, node.tilemap_position.y)
	var runner_position = nodes_map.map_to_world(node_position)
	runner_marker.position = runner_position + Vector2(0, 20)
	runner_marker.visible = true
	runner_node = node
	

# Called when the node enters the scene tree for the first time.
func _ready():
	nodes_map = get_node(nodes_map_path)
	links_map = get_node(links_map_path)
	network_view_camera = get_node(network_view_camera_path)
	runner_marker = get_node(runner_marker_path)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

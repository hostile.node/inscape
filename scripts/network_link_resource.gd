extends Resource
class_name NetworkLinkResource

export(String) var from
export(String) var to
export(Array, Vector2) var tiles


# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(i_from = "", i_to = "", i_tiles = []):
	from = i_from
	to = i_to
	tiles = i_tiles

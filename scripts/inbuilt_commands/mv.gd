extends InbuiltCommand

func execute(arguments):
	if arguments.size() == 0 || arguments.size() > 1:
		gateway.terminal.add_line_error("Usage: mv <node_label>")
	else:
		var node_label = arguments[0]
		var destination_node = null
		for node in gateway.network.nodes:
			if node.label == node_label:
				destination_node = node
				break
		
		if destination_node == null:
			gateway.terminal.add_line_error("Unknown node: " + node_label)
		elif gateway.network.move_runner(destination_node):
			gateway.terminal.add_line("Moving to node " + node_label)
		else:
			gateway.terminal.add_line_error("Access denied.")

extends Node
class_name InbuiltCommand

var gateway = null

func execute(arguments):
	pass


func get_short_description():
	return ""
	
	
func get_long_description():
	return ""


func _ready():
	gateway = get_tree().get_root().find_node("Gateway", true, false)
	assert(gateway is Gateway)
	assert(gateway != null)

extends Resource
class_name NetworkNodeResource

enum NetworkNodeType {NODE, INGRESS, SERVER}

export(String) var label
export(NetworkNodeType) var type
export(int) var grid_x
export(int) var grid_y

# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(i_label = "Template node", i_type = NetworkNodeType.NODE, i_x = 0, i_y = 0):
	label = i_label
	type = i_type
	grid_x = i_x
	grid_y = i_y

extends Node
class_name NetworkNode

var type = NetworkNodeResource.NetworkNodeType.NODE
var label = "placeholder"
var tilemap_position = Vector2(0, 0)
var world_position = Vector2(0, 0)


func _init(var i_network_node_resource : NetworkNodeResource, var i_network_view):
	type = i_network_node_resource.type
	label = i_network_node_resource.label
	tilemap_position.x = i_network_node_resource.grid_x
	tilemap_position.y = i_network_node_resource.grid_y
	
	var node_tilemap = i_network_view.get_node("NodesMap")
	world_position = node_tilemap.map_to_world(tilemap_position)
	
	var node_label = load("res://node_label.tscn").instance()
	i_network_view.add_child(node_label)
	node_label.rect_position = world_position + Vector2(-26, 68)
	node_label.rect_size = Vector2(52, 18)
	node_label.set_text(label)

func _ready():
	pass

#func _process(delta):
#	pass

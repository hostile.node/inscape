extends NinePatchRect


var _label = null


func set_text(var text):
	_label.text = text


# Called when the node enters the scene tree for the first time.
func _ready():
	_label = get_node("Label")
